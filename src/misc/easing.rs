#[derive(Copy, Clone, Debug, Default)]
pub struct Oscillate;
impl keyframe::EasingFunction for Oscillate {
    #[inline]
    fn y(&self, x: f64) -> f64 {
        0.5 - 0.5 * f64::cos(2.0 * std::f64::consts::PI * x)
    }
}
