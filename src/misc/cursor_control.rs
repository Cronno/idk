use std::time::Duration;

use crate::misc::timer::Timer;

pub trait Direction {}
impl Direction for VDirection {}
impl Direction for HDirection {}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum VDirection {
    Up,
    Down,
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum HDirection {
    Left,
    Right,
}

#[derive(Clone)]
pub struct CursorControl<T: Direction> {
    das_timer: Timer,
    arr_timer: Timer,
    prev_dir: Option<T>,
    zero_arr: bool,
}

impl<T: Direction + std::cmp::PartialEq> CursorControl<T> {
    pub fn new(das: Duration, arr: Duration) -> Self {
        Self {
            das_timer: Timer::new(das),
            arr_timer: Timer::new(arr),
            prev_dir: None,
            zero_arr: arr.is_zero(),
        }
    }

    pub fn handle_autoshift(&mut self, dir: Option<T>, delta: Duration) -> usize {
        if self.prev_dir != dir {
            self.arr_timer.reset();
            self.das_timer.reset();
            self.prev_dir = dir;
            return if self.prev_dir.is_some() { 1 } else { 0 };
        }

        if dir.is_none() {
            return 0;
        }

        if !self.das_timer.finished() {
            self.das_timer.tick(delta);
            if self.das_timer.finished() {
                self.auto_repeat(self.das_timer.overflow())
            } else {
                0
            }
        } else {
            self.auto_repeat(delta)
        }
    }

    fn auto_repeat(&mut self, delta: Duration) -> usize {
        if self.zero_arr {
            usize::MAX
        } else {
            self.arr_timer.looping_tick(delta)
        }
    }
}

impl<T: Direction> Default for CursorControl<T> {
    fn default() -> Self {
        Self {
            das_timer: Timer::new(Duration::from_millis(100)),
            arr_timer: Timer::new(Duration::from_millis(50)),
            prev_dir: None,
            zero_arr: false,
        }
    }
}

pub struct CursorControlAbsolute {
    cursor_control: CursorControl<HDirection>,
    current: usize,
    min: usize,
    max: usize,
}

impl CursorControlAbsolute {
    pub fn new(das: Duration, arr: Duration, max: usize) -> Self {
        Self::new_weird(das, arr, 0, 0, max)
    }

    pub fn new_weird(das: Duration, arr: Duration, initial: usize, min: usize, max: usize) -> Self {
        Self {
            cursor_control: CursorControl::new(das, arr),
            current: initial,
            min,
            max,
        }
    }

    pub fn next_index(&mut self, backward: bool, forward: bool, delta: Duration) -> usize {
        let dir = match (backward, forward) {
            (true, false) => Some(HDirection::Left),
            (false, true) => Some(HDirection::Right),
            _ => None,
        };

        let moves = self.cursor_control.handle_autoshift(dir, delta);

        self.current = match dir {
            Some(HDirection::Left) => self.current.saturating_sub(moves).clamp(self.min, self.max),
            Some(HDirection::Right) => self.current.saturating_add(moves).clamp(self.min, self.max),
            None => self.current,
        };

        self.current
    }

    pub fn set_index(&mut self, index: usize) {
        self.current = index.clamp(self.min, self.max);
    }

    pub fn get_index(&self) -> usize {
        self.current.clamp(self.min, self.max)
    }
}

pub fn get_movements(up: bool, down: bool, left: bool, right: bool) -> (Option<VDirection>, Option<HDirection>) {
    let v_dir = match (up, down) {
        (true, false) => Some(VDirection::Up),
        (false, true) => Some(VDirection::Down),
        _ => None,
    };

    let h_dir = match (left, right) {
        (true, false) => Some(HDirection::Left),
        (false, true) => Some(HDirection::Right),
        _ => None,
    };

    (v_dir, h_dir)
}
