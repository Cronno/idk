use macroquad::{
    color::{Color, WHITE},
    math::{Rect, Vec2, vec2},
    shapes::{draw_poly_lines, draw_rectangle, draw_rectangle_lines},
    text::{TextParams, draw_text_ex, measure_text},
    texture::{DrawTextureParams, Texture2D, draw_texture_ex},
};

use crate::assets::graphics::CIRCLE_TEXTURE;

pub fn draw_rect(rect: &Rect, color: Color) {
    draw_rectangle(rect.x, rect.y, rect.w, rect.h, color)
}

pub fn draw_rect_lines(rect: &Rect, thickness: f32, color: Color) {
    draw_rectangle_lines(rect.x, rect.y, rect.w, rect.h, thickness, color)
}

pub fn draw_bordered_box(rect: &Rect, stroke_width: f32, fill_color: Color, stroke_color: Color) {
    let half_stoke = stroke_width / 2.0;
    draw_rectangle(rect.x, rect.y, rect.w, rect.h, fill_color);
    draw_rectangle_lines(
        rect.x - half_stoke,
        rect.y - half_stoke,
        rect.w + stroke_width,
        rect.h + stroke_width,
        stroke_width,
        stroke_color,
    );
}

pub fn draw_nice_circle(center: Vec2, radius: f32, color: Color) {
    let position = center - radius;
    let diameter = radius * 2.0;
    let params = DrawTextureParams {
        dest_size: Some(vec2(diameter, diameter)),
        ..Default::default()
    };

    draw_texture_ex(&CIRCLE_TEXTURE, position.x, position.y, color, params);
}

pub fn draw_nice_circle_lines(center: Vec2, radius: f32, thickness: f32, color: Color) {
    draw_poly_lines(center.x, center.y, 50, radius, 0.0, thickness, color);
}

pub enum TextAlignment {
    Left { margin: f32 },
    Centered,
    Right { margin: f32 },
}

pub fn draw_text_in_rect(text: &str, rect: Rect, alignment: TextAlignment, text_params: TextParams) {
    let font_dimensions = measure_text(
        "8", // Ensures that the height for the font the same regardless of the contents of the text
        text_params.font,
        text_params.font_size,
        text_params.font_scale,
    );
    let dimensions = measure_text(text, text_params.font, text_params.font_size, text_params.font_scale);
    let x_pos = match alignment {
        TextAlignment::Left { margin } => rect.x + margin,
        TextAlignment::Centered => rect.x + rect.w / 2.0 - dimensions.width / 2.0,
        TextAlignment::Right { margin } => rect.x + rect.w - dimensions.width - margin,
    };
    let y_pos = rect.y + rect.h / 2.0 + font_dimensions.height / 2.0;

    draw_text_ex(text, x_pos, y_pos, text_params);
}

pub fn draw_nine_slice(texture: &Texture2D, area: &Rect) {
    let Vec2 { x: width, y: height } = texture.size();

    let third_width = width / 3.0;
    let third_height = height / 3.0;

    let horizontal_stretch = area.w - 2.0 * third_width;
    let vertical_stretch = area.h - 2.0 * third_height;

    let mut dest_y = area.y;
    for i in 0..3 {
        let src_y = third_height * i as f32;
        let dest_h = if i == 1 { vertical_stretch } else { third_height };

        let mut dest_x = area.x;
        for j in 0..3 {
            let src_x = third_width * j as f32;
            let dest_w = if j == 1 { horizontal_stretch } else { third_width };

            draw_texture_ex(
                texture,
                dest_x,
                dest_y,
                WHITE,
                DrawTextureParams {
                    dest_size: Some(vec2(dest_w, dest_h)),
                    source: Some(Rect::new(src_x, src_y, third_width, third_height)),
                    ..Default::default()
                },
            );
            dest_x += dest_w;
        }
        dest_y += dest_h;
    }
}
