use std::time::Duration;

use keyframe::{AnimationSequence, functions::*, keyframes};
use macroquad::{
    camera::Camera2D,
    prelude::{BLACK, Color},
    shapes::draw_rectangle,
};

use crate::{draw::draw_rect, extension::camera::CameraExtension, misc::timer::Timer, scene::director::Scene};

pub(crate) enum AfterTransition {
    PushBoth,
    PushNext,
}

pub(crate) struct Transition<C> {
    animation: Box<dyn TransitionAnim<C>>,
    action: AfterTransition,
    previous_scene: Box<dyn Scene<C>>,
    next_scene: Box<dyn Scene<C>>,
    functions_run: bool,
}

impl<C> Transition<C> {
    pub fn new(
        animation: Box<dyn TransitionAnim<C>>,
        previous_scene: Box<dyn Scene<C>>,
        next_scene: Box<dyn Scene<C>>,
        action: AfterTransition,
    ) -> Self {
        Self {
            animation,
            action,
            previous_scene,
            next_scene,
            functions_run: false,
        }
    }

    pub fn update(&mut self, ctx: &mut C, delta: Duration) {
        self.animation.update(delta);

        if !self.functions_run && self.animation.is_half_finished() {
            self.previous_scene.transition_out(ctx);
            self.next_scene.transition_in(ctx);
            self.functions_run = true;
        }
    }

    pub fn draw(&self, ctx: &C) {
        if !self.animation.is_half_finished() {
            self.previous_scene.render(ctx);
            self.animation.draw(ctx, self.previous_scene.get_camera())
        } else {
            self.next_scene.render(ctx);
            self.animation.draw(ctx, self.next_scene.get_camera())
        }
    }

    pub fn finished(&self) -> bool {
        self.animation.finished()
    }

    pub fn end(self) -> (Box<dyn Scene<C>>, Box<dyn Scene<C>>, AfterTransition) {
        (self.previous_scene, self.next_scene, self.action)
    }

    pub fn current_camera(&self) -> Option<&Camera2D> {
        if !self.animation.is_half_finished() {
            self.previous_scene.get_camera()
        } else {
            self.next_scene.get_camera()
        }
    }
}

pub trait TransitionAnim<C> {
    fn update(&mut self, delta: Duration);
    fn draw(&self, ctx: &C, camera: Option<&Camera2D>);
    fn finished(&self) -> bool;
    fn is_half_finished(&self) -> bool;
}

pub struct Instant {}
impl Instant {
    pub fn base() -> Box<Self> {
        Box::new(Self {})
    }
}

impl<C> TransitionAnim<C> for Instant {
    fn update(&mut self, _delta: Duration) {}

    fn draw(&self, _ctx: &C, _camera: Option<&Camera2D>) {}

    fn finished(&self) -> bool {
        true
    }

    fn is_half_finished(&self) -> bool {
        true
    }
}

pub struct Fade {
    sequence: AnimationSequence<f32>,
    color: Color,
}

impl Fade {
    pub fn new(color: Color) -> Box<Self> {
        let mut base = Self::base();
        base.color = color;
        base
    }

    pub fn base() -> Box<Self> {
        Box::new(Self {
            sequence: keyframes![(0.0, 0.0, Linear), (1.0, 0.2), (1.0, 0.5, Linear), (0.0, 0.7)],
            color: BLACK,
        })
    }
}

impl<C> TransitionAnim<C> for Fade {
    fn update(&mut self, delta: Duration) {
        self.sequence.advance_by(delta.as_secs_f64());
    }

    fn draw(&self, _ctx: &C, camera: Option<&Camera2D>) {
        let mut color = self.color;
        color.a = self.sequence.now();
        draw_rect(&camera.get_screen_rect(), color);
    }

    fn finished(&self) -> bool {
        self.sequence.finished()
    }

    fn is_half_finished(&self) -> bool {
        self.sequence.progress() >= 0.5
    }
}

pub struct WipeRight {
    width_timer: Timer,
    position_timer: Timer,
    color: Color,
}

impl WipeRight {
    pub fn new(color: Color) -> Box<Self> {
        Box::new(Self {
            width_timer: Timer::new(Duration::from_secs_f32(0.6)),
            position_timer: Timer::new(Duration::from_secs_f32(0.6)),
            color,
        })
    }

    pub fn base() -> Box<Self> {
        Box::new(Self {
            width_timer: Timer::new(Duration::from_secs_f32(0.6)),
            position_timer: Timer::new(Duration::from_secs_f32(0.6)),
            color: BLACK,
        })
    }
}

impl<C> TransitionAnim<C> for WipeRight {
    fn update(&mut self, delta: Duration) {
        if !<Self as TransitionAnim<C>>::is_half_finished(self) {
            self.width_timer.tick(delta);
        } else {
            self.position_timer.tick(delta);
        }
    }

    fn draw(&self, _ctx: &C, camera: Option<&Camera2D>) {
        let camera_rect = camera.get_screen_rect();
        if !<Self as TransitionAnim<C>>::is_half_finished(self) {
            let width = keyframe::ease(EaseOut, 0.0, camera_rect.w + 20.0, self.width_timer.percent());
            draw_rectangle(camera_rect.x, camera_rect.y, width, camera_rect.h, self.color);
        } else {
            let x = keyframe::ease(EaseOut, 0.0, camera_rect.w, self.position_timer.percent());
            draw_rectangle(
                camera_rect.x + x,
                camera_rect.y,
                camera_rect.w,
                camera_rect.h,
                self.color,
            );
        }
    }

    fn finished(&self) -> bool {
        self.position_timer.finished()
    }

    fn is_half_finished(&self) -> bool {
        self.width_timer.finished()
    }
}
