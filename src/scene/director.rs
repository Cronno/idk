// 1 no transitions
// 2 next scene cannot consume previous
//     no passing owned data to next scene even when popping
// 3 cannot draw under scenes
// 4 cannot pop multiple scenes
// 5 no communication between scenes
// 6 scenes do not know when they are returned to (fine?)

// if update took ownership of self and returned the next scece (SceneAction::Continue(Box<dyn Scene>))
// SceneAction::Push(Box<dyn Scene>, Box<dyn Scene>)
//
// shared data struct passed to update
// bookmark scene? allow popping back to next bookmark

// Draw under
// bookmark
// pop multiple

use std::time::Duration;

use macroquad::camera::{Camera2D, set_camera};

use super::transition::{AfterTransition, Transition, TransitionAnim};
use crate::{extension::camera::build_screen_camera, input::InputCollector};

pub trait Scene<C> {
    fn handle_input(&mut self, input_events: &InputCollector);
    fn update(&mut self, ctx: &mut C, delta: Duration) -> SceneAction<C>;
    fn render(&self, ctx: &C);
    fn transition_in(&mut self, _ctx: &mut C) {}
    fn transition_out(&mut self, _ctx: &mut C) {}
    fn get_camera(&self) -> Option<&Camera2D> {
        None
    }
}

pub enum SceneAction<C> {
    Continue,
    Push(Box<dyn Scene<C>>, Box<dyn TransitionAnim<C>>),
    Replace(Box<dyn Scene<C>>, Box<dyn TransitionAnim<C>>),
    Pop(Box<dyn TransitionAnim<C>>),
}

pub struct Director<C> {
    stack: Vec<Box<dyn Scene<C>>>,
    transition: Option<Transition<C>>,
}

impl<C> Director<C> {
    pub fn new(start_scene: Box<dyn Scene<C>>) -> Self {
        Director {
            stack: vec![start_scene],
            transition: None,
        }
    }

    pub fn update(&mut self, ctx: &mut C, input_events: &InputCollector, delta: Duration) {
        if let Some(ref mut transition) = self.transition {
            transition.update(ctx, delta);
            if transition.finished() {
                let transition = self.transition.take().unwrap();
                let (previous_scene, next_scene, action) = transition.end();
                match action {
                    AfterTransition::PushBoth => {
                        self.stack.push(previous_scene);
                        self.stack.push(next_scene);
                    }
                    AfterTransition::PushNext => {
                        self.stack.push(next_scene);
                    }
                }
            }
            return;
        }

        let Some(current_scene) = self.stack.last_mut() else {
            return;
        };

        current_scene.handle_input(input_events);
        let action = current_scene.update(ctx, delta);

        match action {
            SceneAction::Continue => {}
            SceneAction::Push(next_scene, transition_anim) => {
                let current_scene = self.stack.pop().unwrap();
                self.transition = Some(Transition::new(
                    transition_anim,
                    current_scene,
                    next_scene,
                    AfterTransition::PushBoth,
                ));
            }
            SceneAction::Replace(next_scene, transition_anim) => {
                let current_scene = self.stack.pop().unwrap();
                self.transition = Some(Transition::new(
                    transition_anim,
                    current_scene,
                    next_scene,
                    AfterTransition::PushNext,
                ));
            }
            SceneAction::Pop(transition_anim) => {
                let current_scene = self.stack.pop().unwrap();
                let next_scene = self.stack.pop().unwrap();
                self.transition = Some(Transition::new(
                    transition_anim,
                    current_scene,
                    next_scene,
                    AfterTransition::PushNext,
                ));
            }
        }
    }

    pub fn render(&self, ctx: &C) {
        match self.current_camera() {
            Some(camera) => set_camera(camera),
            None => set_camera(&build_screen_camera()),
        };

        if let Some(ref transition) = self.transition {
            transition.draw(ctx);
        } else if let Some(current_scene) = self.stack.last() {
            current_scene.render(ctx);
        }
    }

    pub fn current_camera(&self) -> Option<&Camera2D> {
        if let Some(ref transition) = self.transition {
            transition.current_camera()
        } else if let Some(scene) = self.stack.last() {
            scene.get_camera()
        } else {
            None
        }
    }
}
