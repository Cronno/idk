use std::time::Duration;

use enum_map::{Enum, EnumMap};
use macroquad::{
    camera::Camera2D,
    input::mouse_position,
    math::{Rect, Vec2, vec2},
};
use nalgebra::DMatrix;

use super::{
    menu_scene::{ControlMode, MenuInput},
    menu_tile::{self, MenuTile},
};
use crate::{
    input::{InputHandler, MouseInput},
    misc::cursor_control::{CursorControl, HDirection, VDirection, get_movements},
};

pub type TileMenu<T> = EnumMap<T, MenuTile<T>>;

pub trait MenuData {
    type Index: Copy;
    type Value;

    fn horizontal_movement(&self, index: Self::Index, direction: HDirection, distance: usize) -> Self::Index;
    fn vertical_movement(&self, index: Self::Index, direction: VDirection, distance: usize) -> Self::Index;
    fn hovered_index(&self, camera: Option<&Camera2D>) -> Option<Self::Index>;
    fn get(&self, index: Self::Index) -> Option<&Self::Value>;
}

pub struct MenuControl<T: MenuData> {
    selected: T::Index,
    pub data: T,

    mode: ControlMode,
    v_control: CursorControl<VDirection>,
    h_control: CursorControl<HDirection>,
}

impl<T: MenuData> MenuControl<T> {
    pub fn new(tiles: T, initial_index: T::Index) -> Self {
        Self {
            selected: initial_index,
            data: tiles,
            mode: ControlMode::Button,
            v_control: CursorControl::new(Duration::from_millis(180), Duration::from_millis(120)),
            h_control: CursorControl::new(Duration::from_millis(180), Duration::from_millis(120)),
        }
    }

    pub fn update(&mut self, inputs: &InputHandler<MenuInput>, delta: Duration) {
        self.update_mode(inputs);
        match self.mode {
            ControlMode::Button => self.button_mode_update(inputs, delta),
            ControlMode::Mouse => {}
        }
    }

    fn update_mode(&mut self, inputs: &InputHandler<MenuInput>) {
        if inputs.pressed(MenuInput::Up)
            || inputs.pressed(MenuInput::Down)
            || inputs.pressed(MenuInput::Left)
            || inputs.pressed(MenuInput::Right)
            || inputs.pressed(MenuInput::Confirm)
            || inputs.pressed(MenuInput::Cancel)
        {
            self.mode = ControlMode::Button;
        } else if inputs.mouse_delta().length() > 3.0 || inputs.mouse_down(MouseInput::Left) {
            self.mode = ControlMode::Mouse;
        }
    }

    fn button_mode_update(&mut self, inputs: &InputHandler<MenuInput>, delta: Duration) {
        let (v_dir, h_dir) = get_movements(
            inputs.down(MenuInput::Up),
            inputs.down(MenuInput::Down),
            inputs.down(MenuInput::Left),
            inputs.down(MenuInput::Right),
        );
        let v_dist = self.v_control.handle_autoshift(v_dir, delta);
        let h_dist = self.h_control.handle_autoshift(h_dir, delta);

        if let Some(v_dir) = v_dir {
            self.selected = self.data.vertical_movement(self.selected, v_dir, v_dist);
        }

        if let Some(h_dir) = h_dir {
            self.selected = self.data.horizontal_movement(self.selected, h_dir, h_dist);
        }
    }

    pub fn selected_index(&self, camera: Option<&Camera2D>) -> Option<T::Index> {
        match self.mode {
            ControlMode::Button => Some(self.selected),
            ControlMode::Mouse => self.data.hovered_index(camera),
        }
    }

    pub fn selected_value(&self, camera: Option<&Camera2D>) -> Option<&T::Value> {
        match self.selected_index(camera) {
            Some(index) => self.data.get(index),
            None => None,
        }
    }
}

impl<T: Enum + Copy> MenuData for TileMenu<T> {
    type Index = T;
    type Value = MenuTile<T>;

    fn horizontal_movement(&self, mut index: Self::Index, direction: HDirection, distance: usize) -> Self::Index {
        for _ in 0..distance {
            index = match self[index].horizontal_neighbor(direction) {
                Some(i) => i,
                None => break,
            }
        }
        index
    }

    fn vertical_movement(&self, mut index: Self::Index, direction: VDirection, distance: usize) -> Self::Index {
        for _ in 0..distance {
            index = match self[index].vertical_neighbor(direction) {
                Some(i) => i,
                None => break,
            }
        }
        index
    }

    fn hovered_index(&self, camera: Option<&Camera2D>) -> Option<Self::Index> {
        menu_tile::hovered_tile(self, camera)
    }

    fn get(&self, index: Self::Index) -> Option<&Self::Value> {
        Some(&self[index])
    }
}

pub struct GridMenuPosition {
    pub top_left: Vec2,
    pub tile_width: f32,
    pub tile_height: f32,
    pub margin_x: f32,
    pub margin_y: f32,
}

impl GridMenuPosition {
    fn full_tile_width(&self) -> f32 {
        self.tile_width + self.margin_x
    }

    fn full_tile_height(&self) -> f32 {
        self.tile_height + self.margin_y
    }
}

pub struct GridMenu<T> {
    pub grid: DMatrix<T>,
    pub position: GridMenuPosition,
}

impl<T> GridMenu<T> {
    pub fn new(matrix: DMatrix<T>, position: GridMenuPosition) -> Self {
        Self { grid: matrix, position }
    }

    pub fn full_grid_rect(&self) -> Rect {
        Rect::new(
            self.position.top_left.x,
            self.position.top_left.y,
            self.grid.ncols() as f32 * self.position.full_tile_width() + self.position.margin_x,
            self.grid.nrows() as f32 * self.position.full_tile_height() + self.position.margin_y,
        )
    }

    pub fn tile_top_left(&self, row: usize, col: usize) -> Vec2 {
        vec2(
            col as f32 * self.position.full_tile_width() + self.position.top_left.x + self.position.margin_x,
            row as f32 * self.position.full_tile_height() + self.position.top_left.y + self.position.margin_y,
        )
    }

    pub fn tile_rect(&self, row: usize, col: usize) -> Rect {
        let coords = self.tile_top_left(row, col);
        Rect::new(coords.x, coords.y, self.position.tile_width, self.position.tile_height)
    }

    pub fn iter_indices(&self) -> impl Iterator<Item = (usize, usize)> + '_ {
        (0..self.grid.ncols()).flat_map(|col| (0..self.grid.nrows()).map(move |row| (row, col)))
    }
}

impl<T> MenuData for GridMenu<T> {
    type Index = (usize, usize);
    type Value = T;

    fn horizontal_movement(&self, index: Self::Index, direction: HDirection, distance: usize) -> Self::Index {
        let (row, mut col) = index;
        col = match direction {
            HDirection::Left => col.saturating_sub(distance),
            HDirection::Right => col.saturating_add(distance).min(self.grid.ncols() - 1),
        };

        (row, col)
    }

    fn vertical_movement(&self, index: Self::Index, direction: VDirection, distance: usize) -> Self::Index {
        let (mut row, col) = index;
        row = match direction {
            VDirection::Up => row.saturating_sub(distance),
            VDirection::Down => row.saturating_add(distance).min(self.grid.nrows() - 1),
        };

        (row, col)
    }

    fn hovered_index(&self, camera: Option<&Camera2D>) -> Option<Self::Index> {
        let mouse_position = match camera {
            Some(camera) => camera.screen_to_world(mouse_position().into()),
            None => mouse_position().into(),
        };

        if self.full_grid_rect().contains(mouse_position) {
            let col = ((mouse_position.x - self.position.top_left.x) / self.position.full_tile_width()) as usize;
            let row = ((mouse_position.y - self.position.top_left.y) / self.position.full_tile_height()) as usize;

            if self.grid.get((row, col)).is_some() && self.tile_rect(row, col).contains(mouse_position) {
                Some((row, col))
            } else {
                None
            }
        } else {
            None
        }
    }

    fn get(&self, index: Self::Index) -> Option<&Self::Value> {
        self.grid.get(index)
    }
}
