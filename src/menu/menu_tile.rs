use enum_map::{Enum, EnumMap};
use macroquad::{
    camera::Camera2D,
    math::Vec2,
    prelude::{Rect, mouse_position},
    window::{screen_height, screen_width},
};
use serde::{Deserialize, Serialize};

use crate::{
    extension::{
        camera::{LOGICAL_SCREEN_HEIGHT, LOGICAL_SCREEN_WIDTH},
        rect::RectDef,
    },
    misc::cursor_control::{HDirection, VDirection},
};

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct NeighborTiles<T> {
    pub above: Option<T>,
    pub below: Option<T>,
    pub left: Option<T>,
    pub right: Option<T>,
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct MenuTile<T: Copy> {
    #[serde(with = "RectDef")]
    pub area: Rect,
    pub neighbors: NeighborTiles<T>,
}

impl<T: Copy> MenuTile<T> {
    pub fn new(area: Rect, neighbors: NeighborTiles<T>) -> Self {
        Self { area, neighbors }
    }

    pub fn vertical_neighbor(&self, dir: VDirection) -> Option<T> {
        match dir {
            VDirection::Up => self.neighbors.above,
            VDirection::Down => self.neighbors.below,
        }
    }

    pub fn horizontal_neighbor(&self, dir: HDirection) -> Option<T> {
        match dir {
            HDirection::Left => self.neighbors.left,
            HDirection::Right => self.neighbors.right,
        }
    }
}

#[derive(Debug)]
pub struct ValueTile {
    value: isize,
    min: isize,
    max: isize,
}

impl ValueTile {
    pub fn new(value: isize, min: isize, max: isize) -> Self {
        Self { value, min, max }
    }

    pub fn modify_value(&mut self, change: isize) {
        self.value += change;
        self.value = self.value.clamp(self.min, self.max);
    }

    pub fn get_value(&self) -> isize {
        self.value
    }
}

pub fn hovered_tile<T>(tiles: &EnumMap<T, MenuTile<T>>, camera: Option<&Camera2D>) -> Option<T>
where
    T: Enum + Copy,
{
    let mouse_position = match camera {
        Some(camera) => camera.screen_to_world(mouse_position().into()),
        None => {
            let mut mouse_position: Vec2 = mouse_position().into();
            mouse_position.x *= LOGICAL_SCREEN_WIDTH / screen_width();
            mouse_position.y *= LOGICAL_SCREEN_HEIGHT / screen_height();
            mouse_position
        }
    };
    for (name, data) in tiles {
        if data.area.contains(mouse_position) {
            return Some(name);
        }
    }
    None
}
