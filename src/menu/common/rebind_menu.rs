use std::{collections::HashMap, fmt::Display, hash::Hash, marker::PhantomData, time::Duration};

use enum_map::{Enum, EnumMap};
use macroquad::prelude::*;
use strum::{EnumCount, IntoEnumIterator};

use super::scrollbar::{ScrollBar, ScrollBarStyle};
use crate::{
    draw::{TextAlignment, draw_bordered_box, draw_rect, draw_text_in_rect},
    extension::{
        camera::{LOGICAL_SCREEN_HEIGHT, LOGICAL_SCREEN_WIDTH},
        color::{ColorExtension, hex_rgb},
    },
    input::{InputCollector, InputEvent, InputHandler, InputState, InputType},
    menu::{
        menu_scene::{ControlMode, Menu, MenuInput},
        menu_tile::{MenuTile, NeighborTiles, hovered_tile},
    },
    misc::cursor_control::{HDirection, VDirection},
    scene::{director::SceneAction, transition::Fade},
};

pub trait Rebindable<S>: Enum + EnumCount + IntoEnumIterator + Display + Copy + Eq + Hash {
    fn update_settings(settings: &mut S, bindings: &HashMap<Self, Vec<InputType>>);
    fn default() -> HashMap<Self, Vec<InputType>>;
}

pub trait RebindContext {
    type Settings;

    fn rebind_style(&self) -> RebindMenuStyle;
    fn play_confirm_sfx(&self);
    fn play_cancel_sfx(&self);
    fn play_movement_sfx(&self);
    fn settings(&mut self) -> &mut Self::Settings;
}

pub struct RebindMenuStyle<'a> {
    pub colors: RebindMenuColors,
    pub scrollbar_style: ScrollBarStyle,
    pub add_icon: &'a Texture2D,
    pub remove_icon: &'a Texture2D,
    pub background: &'a Texture2D,
    pub font: Option<&'a Font>,
    pub monospace_font: Option<&'a Font>,
}

pub struct RebindMenuColors {
    pub unselected: Color,
    pub highlight: Color,
    pub tile: Color,
    pub accent: Color,
}

impl Default for RebindMenuColors {
    fn default() -> Self {
        Self {
            unselected: color_u8!(127, 143, 157, 255),
            highlight: color_u8!(181, 191, 202, 255),
            tile: color_u8!(137, 143, 146, 255),
            accent: WHITE,
        }
    }
}

enum State {
    Normal,
    Binding { pressed_key: Option<InputType> },
    Error { message: String },
}

const HEADER_SIZE: f32 = 50.0;
const ROW_HEIGHT: f32 = 150.0;

#[derive(Debug, Default, Clone, Copy, Enum, PartialEq, Eq)]
enum RebindTile {
    Add(u8),
    Remove(u8),
    #[default]
    Apply,
    Cancel,
    Default,
}

pub struct RebindMenu<T: Rebindable<S>, S> {
    state: State,
    paused: bool,
    scrollbar: ScrollBar,
    new_bindings: HashMap<T, Vec<InputType>>,

    tiles: EnumMap<RebindTile, MenuTile<RebindTile>>,
    selected_tile: RebindTile,
    input_states: HashMap<InputType, InputState>,

    _phantom_data: PhantomData<S>,
}

impl<T: Rebindable<S>, S> RebindMenu<T, S> {
    const ROW_COUNT: usize = T::COUNT + 1;

    pub fn new(bindings: &HashMap<T, Vec<InputType>>) -> Self {
        let mut scrollbar = ScrollBar::new(Self::ROW_COUNT, ROW_HEIGHT, HEADER_SIZE);
        scrollbar.camera.target.y = LOGICAL_SCREEN_HEIGHT / 2.0;

        let tiles = Self::generate_rebind_tiles();

        let mut new_bindings = bindings.clone();

        for input in T::iter() {
            new_bindings.entry(input).or_default();
        }

        Self {
            state: State::Normal,
            paused: false,
            scrollbar,
            new_bindings,
            tiles,
            selected_tile: RebindTile::Add(0),
            input_states: HashMap::new(),

            _phantom_data: PhantomData {},
        }
    }

    fn generate_rebind_tiles() -> EnumMap<RebindTile, MenuTile<RebindTile>> {
        let mut tiles = EnumMap::default();

        for i in 0..T::COUNT as u8 {
            let y = HEADER_SIZE + ROW_HEIGHT * i as f32;
            tiles[RebindTile::Add(i)] = MenuTile::new(
                Rect::new(400.0, y, 125.0, 100.0),
                NeighborTiles {
                    above: Some(RebindTile::Add(i.saturating_sub(1))),
                    below: Some(RebindTile::Add(i + 1)),
                    left: None,
                    right: Some(RebindTile::Remove(i)),
                },
            );
            tiles[RebindTile::Remove(i)] = MenuTile::new(
                Rect::new(525.0, y, 125.0, 100.0),
                NeighborTiles {
                    above: Some(RebindTile::Remove(i.saturating_sub(1))),
                    below: Some(RebindTile::Remove(i + 1)),
                    left: Some(RebindTile::Add(i)),
                    right: None,
                },
            );
        }

        let last_control_row = T::COUNT as u8 - 1;
        tiles[RebindTile::Add(0)].neighbors.above = None;
        tiles[RebindTile::Remove(0)].neighbors.above = None;
        tiles[RebindTile::Add(last_control_row)].neighbors.below = Some(RebindTile::Apply);
        tiles[RebindTile::Remove(last_control_row)].neighbors.below = Some(RebindTile::Cancel);

        let y = (Self::ROW_COUNT - 1) as f32 * ROW_HEIGHT + HEADER_SIZE;
        tiles[RebindTile::Apply] = MenuTile::new(
            Rect::new(100.0, y, 325.0, 100.0),
            NeighborTiles {
                above: Some(RebindTile::Add(last_control_row)),
                below: None,
                left: None,
                right: Some(RebindTile::Cancel),
            },
        );

        tiles[RebindTile::Cancel] = MenuTile::new(
            Rect::new(475.0, y, 325.0, 100.0),
            NeighborTiles {
                above: Some(RebindTile::Remove(last_control_row)),
                below: None,
                left: Some(RebindTile::Apply),
                right: Some(RebindTile::Default),
            },
        );

        tiles[RebindTile::Default] = MenuTile::new(
            Rect::new(850.0, y, 325.0, 100.0),
            NeighborTiles {
                above: Some(RebindTile::Remove(last_control_row)),
                below: None,
                left: Some(RebindTile::Cancel),
                right: None,
            },
        );

        tiles
    }

    fn current_row(&self) -> usize {
        match self.selected_tile {
            RebindTile::Add(row) => row as usize,
            RebindTile::Remove(row) => row as usize,
            _ => Self::ROW_COUNT - 1,
        }
    }

    fn rebind_checks(&mut self) -> bool {
        let empty_binding = self.new_bindings.iter().find(|(_, bindings)| bindings.is_empty());
        if let Some((empty_input, _)) = empty_binding {
            self.state = State::Error {
                message: format!("No bindings for: {}", empty_input),
            };
            self.paused = true;
            return false;
        }

        true
    }

    fn draw_controls_row(&self, row: u8, input: T, style: &RebindMenuStyle) {
        let params = TextParams {
            font: style.monospace_font,
            font_size: 36,
            color: WHITE,
            ..Default::default()
        };

        let add_rect = self.tiles[RebindTile::Add(row)].area;
        let remove_rect = self.tiles[RebindTile::Remove(row)].area;

        let name_box = Rect::new(100.0, add_rect.y, 250.0, 100.0);
        let binding_box = Rect::new(700.0, add_rect.y, 475.0, 100.0);

        draw_rect(&name_box, style.colors.tile);
        draw_text_in_rect(
            &input.to_string().to_uppercase(),
            name_box,
            TextAlignment::Left { margin: 20.0 },
            params.clone(),
        );

        let binding_string = self.bindings_string(input);
        draw_rect(&binding_box, style.colors.tile);
        draw_text_in_rect(
            &binding_string.to_uppercase(),
            binding_box,
            TextAlignment::Left { margin: 20.0 },
            params.clone(),
        );

        draw_rect(&add_rect, style.colors.unselected);
        draw_rect(&remove_rect, style.colors.unselected);
        draw_rectangle(518.75, add_rect.y, 12.5, 100.0, hex_rgb(0x414345));
        match self.selected_tile {
            RebindTile::Add(r) if r == row => {
                draw_bordered_box(&add_rect, 12.5, style.colors.highlight, style.colors.accent)
            }
            RebindTile::Remove(r) if r == row => {
                draw_bordered_box(&remove_rect, 12.5, style.colors.highlight, style.colors.accent)
            }
            _ => {}
        };
        draw_texture(style.add_icon, 430.0, 70.0 + ROW_HEIGHT * row as f32, WHITE);
        draw_texture(style.remove_icon, 566.0, 75.0 + ROW_HEIGHT * row as f32, WHITE);
    }

    fn draw_last_row(&self, style: &RebindMenuStyle) {
        let params = TextParams {
            font: style.monospace_font,
            font_size: 36,
            color: WHITE,
            ..Default::default()
        };

        draw_rect(&self.tiles[RebindTile::Apply].area, style.colors.unselected);
        draw_rect(&self.tiles[RebindTile::Cancel].area, style.colors.unselected);
        draw_rect(&self.tiles[RebindTile::Default].area, style.colors.unselected);

        match self.selected_tile {
            RebindTile::Apply | RebindTile::Cancel | RebindTile::Default => draw_bordered_box(
                &self.tiles[self.selected_tile].area,
                12.5,
                style.colors.highlight,
                style.colors.accent,
            ),
            _ => {}
        }

        draw_text_in_rect(
            "APPLY",
            self.tiles[RebindTile::Apply].area,
            TextAlignment::Centered,
            params.clone(),
        );
        draw_text_in_rect(
            "CANCEL",
            self.tiles[RebindTile::Cancel].area,
            TextAlignment::Centered,
            params.clone(),
        );
        draw_text_in_rect(
            "DEFAULT",
            self.tiles[RebindTile::Default].area,
            TextAlignment::Centered,
            params.clone(),
        );
    }

    fn draw_error_box(&self, message: &str, style: &RebindMenuStyle) {
        let params = TextParams {
            font: style.font,
            font_size: 48,
            color: WHITE,
            ..Default::default()
        };

        draw_message_box(self.scrollbar.camera_top(), style);
        let text_area = Rect::new(75.0, 75.0 + self.scrollbar.camera_top(), 1130.0, 570.0);
        draw_text_in_rect(message, text_area, TextAlignment::Centered, params);
    }

    fn draw_binding_box(&self, style: &RebindMenuStyle) {
        let params = TextParams {
            font: style.font,
            font_size: 42,
            color: WHITE,
            ..Default::default()
        };

        draw_message_box(self.scrollbar.camera_top(), style);

        let message = "Press and release a key to bind";
        let top_rect = Rect::new(75.0, 75.0 + self.scrollbar.camera_top(), 1130.0, 150.0);
        draw_text_in_rect(message, top_rect, TextAlignment::Centered, params.clone());

        let input = T::iter().nth(self.current_row()).unwrap();
        let name_box = Rect::new(180.0, 306.0 + self.scrollbar.camera_top(), 216.0, 108.0);
        draw_rectangle(name_box.x, name_box.y, name_box.w, name_box.h, hex_rgb(0x414345));
        draw_text_in_rect(
            &input.to_string().to_uppercase(),
            name_box,
            TextAlignment::Left { margin: 20.0 },
            params.clone(),
        );
        let binding_string = self.bindings_string(input);
        let binding_box = Rect::new(587.0, 306.0 + self.scrollbar.camera_top(), 513.0, 108.0);
        draw_rectangle(
            binding_box.x,
            binding_box.y,
            binding_box.w,
            binding_box.h,
            hex_rgb(0x414345),
        );
        draw_text_in_rect(
            &binding_string.to_uppercase(),
            binding_box,
            TextAlignment::Left { margin: 20.0 },
            params.clone(),
        );

        let message = "Press any two keys to cancel";
        let bottom_rect = Rect::new(75.0, 495.0 + self.scrollbar.camera_top(), 1130.0, 150.0);
        draw_text_in_rect(message, bottom_rect, TextAlignment::Centered, params);
    }

    fn bindings_string(&self, input: T) -> String {
        let mut binding_string = String::new();
        for input in &self.new_bindings[&input] {
            match input {
                InputType::Key(key) => binding_string += keys::keycode_to_string(*key),
                InputType::Btn(btn) => binding_string += &format!("Btn({})", keys::button_to_string(*btn)),
            }
            binding_string += "|";
        }

        let mut binding_string = binding_string.trim_end_matches('|').to_string();

        if binding_string.len() > 20 {
            binding_string = binding_string[0..17].to_string() + "...";
        }
        binding_string
    }
}

impl<C: RebindContext<Settings = S>, T: Rebindable<S>, S> Menu<C> for RebindMenu<T, S> {
    fn handle_input(&mut self, input_events: &InputCollector) {
        for (_, state) in self.input_states.iter_mut() {
            if *state == InputState::Pressed {
                *state = InputState::Down;
            } else if *state == InputState::Released {
                *state = InputState::Up;
            }
        }

        for (input, event) in input_events.input_events.iter() {
            let state = match event {
                InputEvent::Pressed => InputState::Pressed,
                InputEvent::Released => InputState::Released,
            };
            self.input_states.insert(*input, state);
        }
    }

    fn update(&mut self, ctx: &mut C, inputs: &InputHandler<MenuInput>, mode: ControlMode, delta: Duration) {
        let current_row = self.current_row();
        match self.state {
            State::Normal => {
                self.paused = false;
                self.scrollbar.update_camera(inputs, mode, current_row, delta);
                if mode == ControlMode::Mouse {
                    self.selected_tile =
                        hovered_tile(&self.tiles, Some(&self.scrollbar.camera)).unwrap_or(self.selected_tile);
                }
            }
            State::Binding { ref mut pressed_key } => match pressed_key {
                None => {
                    let new_pressed_key = self
                        .input_states
                        .iter()
                        .find(|&(_, &s)| s == InputState::Down || s == InputState::Pressed)
                        .map(|(i, _)| i);
                    if let Some(input) = new_pressed_key {
                        *pressed_key = Some(*input);
                        ctx.play_confirm_sfx();
                    }
                }
                Some(input) => {
                    if *self.input_states.entry(*input).or_default() == InputState::Released {
                        if let Some(bindings) = T::iter().nth(current_row).and_then(|x| self.new_bindings.get_mut(&x)) {
                            if !bindings.contains(input) {
                                bindings.push(*input);
                            }
                        }
                        self.state = State::Normal;
                    } else if self
                        .input_states
                        .iter()
                        .any(|(i, s)| (*s == InputState::Down || *s == InputState::Pressed) && i != input)
                    {
                        ctx.play_cancel_sfx();
                        self.state = State::Normal;
                    }
                }
            },
            State::Error { message: _ } => {}
        }
    }

    fn confirm(&mut self, ctx: &mut C, mode: ControlMode, _delta: Duration) -> SceneAction<C> {
        if self.paused {
            if matches!(self.state, State::Error { message: _ }) {
                ctx.play_confirm_sfx();
                self.paused = false;
                self.state = State::Normal;
            }
            return SceneAction::Continue;
        }

        let selected = match mode {
            ControlMode::Button => Some(self.selected_tile),
            ControlMode::Mouse => hovered_tile(&self.tiles, Some(&self.scrollbar.camera)),
        };

        match selected {
            Some(RebindTile::Add(_)) => {
                ctx.play_confirm_sfx();
                self.paused = true;
                self.input_states.clear();
                self.state = State::Binding { pressed_key: None };
                SceneAction::Continue
            }
            Some(RebindTile::Remove(row)) => {
                ctx.play_cancel_sfx();
                if let Some(bindings) = T::iter().nth(row as usize).and_then(|x| self.new_bindings.get_mut(&x)) {
                    bindings.pop();
                }
                SceneAction::Continue
            }
            Some(RebindTile::Apply) => {
                ctx.play_confirm_sfx();
                if self.rebind_checks() {
                    T::update_settings(ctx.settings(), &self.new_bindings);
                    SceneAction::Pop(Fade::new(hex_rgb(0x686C6F)))
                } else {
                    SceneAction::Continue
                }
            }
            Some(RebindTile::Cancel) => {
                ctx.play_confirm_sfx();
                SceneAction::Pop(Fade::new(hex_rgb(0x686C6F)))
            }
            Some(RebindTile::Default) => {
                ctx.play_confirm_sfx();
                self.new_bindings = T::default();
                SceneAction::Continue
            }
            None => SceneAction::Continue,
        }
    }

    fn cancel(&mut self, _ctx: &mut C, _delta: Duration) -> SceneAction<C> {
        if !self.paused {
            self.selected_tile = RebindTile::Apply;
            self.scrollbar.scroll_to_bottom();
        }
        SceneAction::Continue
    }

    fn vertical_movement(&mut self, ctx: &C, dir: VDirection, _inputs: &InputHandler<MenuInput>, _delta: Duration) {
        if self.paused {
            return;
        };

        let prev_selected = self.selected_tile;

        self.selected_tile = self.tiles[self.selected_tile]
            .vertical_neighbor(dir)
            .unwrap_or(self.selected_tile);

        if self.selected_tile != prev_selected {
            ctx.play_movement_sfx();
        }
    }

    fn horizontal_movement(&mut self, ctx: &C, dir: HDirection, _inputs: &InputHandler<MenuInput>, _delta: Duration) {
        if self.paused {
            return;
        };

        let prev_selected = self.selected_tile;

        self.selected_tile = self.tiles[self.selected_tile]
            .horizontal_neighbor(dir)
            .unwrap_or(self.selected_tile);

        if self.selected_tile != prev_selected {
            ctx.play_movement_sfx();
        }
    }

    fn render(&self, ctx: &C, _mode: ControlMode) {
        let style = ctx.rebind_style();

        let top_left = self.scrollbar.camera.screen_to_world(vec2(0.0, 0.0));
        draw_texture(style.background, top_left.x, top_left.y, WHITE);

        for (row, input) in T::iter().enumerate() {
            self.draw_controls_row(row as u8, input, &style);
        }

        self.draw_last_row(&style);

        self.scrollbar.draw_scroll_bar(&style.scrollbar_style);
        match self.state {
            State::Error { ref message } => self.draw_error_box(message, &style),
            State::Binding { pressed_key: _, .. } => self.draw_binding_box(&style),
            _ => {}
        };

        // draw_texture(ctx.assets.gfx(Gfx::Gradient), top_left.x, top_left.y, WHITE);
    }

    fn get_camera(&self) -> Option<&Camera2D> {
        Some(&self.scrollbar.camera)
    }
}

fn draw_message_box(camera_top: f32, style: &RebindMenuStyle) {
    draw_rectangle(
        0.0,
        camera_top,
        LOGICAL_SCREEN_WIDTH,
        camera_top + LOGICAL_SCREEN_HEIGHT,
        BLACK.with_alpha(0.6),
    );
    let margin = 75.0;
    draw_rectangle(
        margin,
        margin + camera_top,
        LOGICAL_SCREEN_WIDTH - margin * 2.0,
        LOGICAL_SCREEN_HEIGHT - margin * 2.0,
        style.colors.tile,
    );
}

mod keys {
    use gilrs::Button;
    use macroquad::miniquad::KeyCode;

    pub fn keycode_to_string(key: KeyCode) -> &'static str {
        match key {
            KeyCode::Space => "Space",
            KeyCode::Apostrophe => "'",
            KeyCode::Comma => ",",
            KeyCode::Minus => "-",
            KeyCode::Period => ".",
            KeyCode::Slash => "/",
            KeyCode::Key0 => "0",
            KeyCode::Key1 => "1",
            KeyCode::Key2 => "2",
            KeyCode::Key3 => "3",
            KeyCode::Key4 => "4",
            KeyCode::Key5 => "5",
            KeyCode::Key6 => "6",
            KeyCode::Key7 => "7",
            KeyCode::Key8 => "8",
            KeyCode::Key9 => "9",
            KeyCode::Semicolon => ";",
            KeyCode::Equal => "=",
            KeyCode::A => "A",
            KeyCode::B => "B",
            KeyCode::C => "C",
            KeyCode::D => "D",
            KeyCode::E => "E",
            KeyCode::F => "F",
            KeyCode::G => "G",
            KeyCode::H => "H",
            KeyCode::I => "I",
            KeyCode::J => "J",
            KeyCode::K => "K",
            KeyCode::L => "L",
            KeyCode::M => "M",
            KeyCode::N => "N",
            KeyCode::O => "O",
            KeyCode::P => "P",
            KeyCode::Q => "Q",
            KeyCode::R => "R",
            KeyCode::S => "S",
            KeyCode::T => "T",
            KeyCode::U => "U",
            KeyCode::V => "V",
            KeyCode::W => "W",
            KeyCode::X => "X",
            KeyCode::Y => "Y",
            KeyCode::Z => "Z",
            KeyCode::LeftBracket => "[",
            KeyCode::Backslash => "\\",
            KeyCode::RightBracket => "]",
            KeyCode::GraveAccent => "`",
            KeyCode::World1 => "World1",
            KeyCode::World2 => "World2",
            KeyCode::Escape => "Esc",
            KeyCode::Enter => "Enter",
            KeyCode::Tab => "Tab",
            KeyCode::Backspace => "Backspace",
            KeyCode::Insert => "Insert",
            KeyCode::Delete => "Delete",
            KeyCode::Right => "Right",
            KeyCode::Left => "Left",
            KeyCode::Down => "Down",
            KeyCode::Up => "Up",
            KeyCode::PageUp => "PageUp",
            KeyCode::PageDown => "PageDown",
            KeyCode::Home => "Home",
            KeyCode::End => "End",
            KeyCode::CapsLock => "CapsLock",
            KeyCode::ScrollLock => "ScrollLock",
            KeyCode::NumLock => "NumLock",
            KeyCode::PrintScreen => "PrintScreen",
            KeyCode::Pause => "Pause",
            KeyCode::F1 => "F1",
            KeyCode::F2 => "F2",
            KeyCode::F3 => "F3",
            KeyCode::F4 => "F4",
            KeyCode::F5 => "F5",
            KeyCode::F6 => "F6",
            KeyCode::F7 => "F7",
            KeyCode::F8 => "F8",
            KeyCode::F9 => "F9",
            KeyCode::F10 => "F10",
            KeyCode::F11 => "F11",
            KeyCode::F12 => "F12",
            KeyCode::F13 => "F13",
            KeyCode::F14 => "F14",
            KeyCode::F15 => "F15",
            KeyCode::F16 => "F16",
            KeyCode::F17 => "F17",
            KeyCode::F18 => "F18",
            KeyCode::F19 => "F19",
            KeyCode::F20 => "F20",
            KeyCode::F21 => "F21",
            KeyCode::F22 => "F22",
            KeyCode::F23 => "F23",
            KeyCode::F24 => "F24",
            KeyCode::F25 => "F25",
            KeyCode::Kp0 => "0",
            KeyCode::Kp1 => "1",
            KeyCode::Kp2 => "2",
            KeyCode::Kp3 => "3",
            KeyCode::Kp4 => "4",
            KeyCode::Kp5 => "5",
            KeyCode::Kp6 => "6",
            KeyCode::Kp7 => "7",
            KeyCode::Kp8 => "8",
            KeyCode::Kp9 => "9",
            KeyCode::KpDecimal => ".",
            KeyCode::KpDivide => "/",
            KeyCode::KpMultiply => "*",
            KeyCode::KpSubtract => "-",
            KeyCode::KpAdd => "+",
            KeyCode::KpEnter => "Enter",
            KeyCode::KpEqual => "KpEqual",
            KeyCode::LeftShift => "LeftShift",
            KeyCode::LeftControl => "LeftControl",
            KeyCode::LeftAlt => "LeftAlt",
            KeyCode::LeftSuper => "LeftSuper",
            KeyCode::RightShift => "RightShift",
            KeyCode::RightControl => "RightControl",
            KeyCode::RightAlt => "RightAlt",
            KeyCode::RightSuper => "RightSuper",
            KeyCode::Menu => "Menu",
            KeyCode::Unknown => "Unknown",
        }
    }

    pub fn button_to_string(btn: Button) -> &'static str {
        match btn {
            Button::South => "B",
            Button::East => "A",
            Button::North => "X",
            Button::West => "Y",
            Button::C => "C",
            Button::Z => "Z",
            Button::LeftTrigger => "L",
            Button::LeftTrigger2 => "L2",
            Button::RightTrigger => "R",
            Button::RightTrigger2 => "R2",
            Button::Select => "Select",
            Button::Start => "Start",
            Button::Mode => "Home",
            Button::LeftThumb => "L3",
            Button::RightThumb => "R3",
            Button::DPadUp => "Up",
            Button::DPadDown => "Down",
            Button::DPadLeft => "Left",
            Button::DPadRight => "Right",
            Button::Unknown => "Unknown",
        }
    }
}
