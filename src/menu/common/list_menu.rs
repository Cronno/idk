use enum_map::{Enum, EnumMap};
use macroquad::math::{Rect, Vec2};
use strum::{EnumCount, IntoEnumIterator};

use crate::menu::menu_tile::{MenuTile, NeighborTiles};

pub fn generate_list_tiles<T>(
    tile_size: Vec2,
    top_left: Vec2,
    bottom_margin: f32,
    paging: bool,
    wrapping: bool,
) -> EnumMap<T, MenuTile<T>>
where
    T: Enum + IntoEnumIterator + EnumCount + Copy + Default,
{
    let mut tiles = EnumMap::default();

    let row_height = tile_size.y + bottom_margin;

    let tile_list: Vec<_> = T::iter().collect();
    for (i, tile) in tile_list.iter().enumerate() {
        let y = top_left.y + row_height * i as f32;

        let above = match (tile_list.get(i.wrapping_sub(1)), wrapping) {
            (Some(t), _) => Some(*t),
            (None, true) => Some(T::from_usize(T::COUNT - 1)),
            (None, false) => None,
        };

        let below = match (tile_list.get(i.wrapping_add(1)), wrapping) {
            (Some(t), _) => Some(*t),
            (None, true) => Some(T::from_usize(0)),
            (None, false) => None,
        };

        let left = match (tile_list.get(i.wrapping_sub(5)), paging) {
            (_, false) => None,
            (Some(t), true) => Some(*t),
            (None, true) => Some(T::from_usize(0)),
        };

        let right = match (tile_list.get(i.wrapping_add(5)), paging) {
            (_, false) => None,
            (Some(t), true) => Some(*t),
            (None, true) => Some(T::from_usize(T::COUNT - 1)),
        };

        tiles[*tile] = MenuTile::new(
            Rect::new(top_left.x, y, tile_size.x, tile_size.y),
            NeighborTiles {
                above,
                below,
                left,
                right,
            },
        );
    }

    tiles
}
