use std::time::Duration;

use enum_map::Enum;
use macroquad::{camera::Camera2D, color::Color, shapes::draw_rectangle};

use crate::{
    extension::{
        camera::{LOGICAL_SCREEN_HEIGHT, LOGICAL_SCREEN_WIDTH, build_screen_camera},
        color::hex_rgb,
    },
    input::{InputHandler, MouseWheel},
    menu::menu_scene::ControlMode,
};

pub struct ScrollBarStyle {
    pub width: f32,
    pub bg_color: Color,
    pub bar_color: Color,
}

impl Default for ScrollBarStyle {
    fn default() -> Self {
        Self {
            width: 25.0,
            bg_color: hex_rgb(0x414345),
            bar_color: hex_rgb(0x898F92),
        }
    }
}

enum ScrollState {
    Normal,
    ToTop,
    ToBottom,
}

pub struct ScrollBar {
    pub camera: Camera2D,
    row_count: usize,
    row_height: f32,
    header_size: f32,
    scroll_state: ScrollState,
}

impl ScrollBar {
    pub fn new(row_count: usize, row_height: f32, header_size: f32) -> Self {
        Self {
            camera: build_screen_camera(),
            row_count,
            row_height,
            header_size,
            scroll_state: ScrollState::Normal,
        }
    }

    pub fn update_camera<T: Enum + Copy>(
        &mut self,
        inputs: &InputHandler<T>,
        mode: ControlMode,
        current_row: usize,
        delta: Duration,
    ) {
        match self.scroll_state {
            ScrollState::Normal => match mode {
                ControlMode::Button => self.button_update(current_row, delta),
                ControlMode::Mouse => self.mouse_update(inputs),
            },
            ScrollState::ToTop => {
                self.button_update(0, delta);
                if self.at_top() {
                    self.scroll_state = ScrollState::Normal;
                }
            }
            ScrollState::ToBottom => {
                self.button_update(self.row_count - 1, delta);
                if self.at_bottom() {
                    self.scroll_state = ScrollState::Normal;
                }
            }
        }
    }

    fn button_update(&mut self, current_row: usize, delta: Duration) {
        let scroll_speed = 1000.0 * delta.as_secs_f32();
        if current_row == 0 {
            self.camera.target.y = move_toward(self.camera.target.y, LOGICAL_SCREEN_HEIGHT / 2.0, scroll_speed);
        } else if current_row == self.row_count - 1 {
            self.camera.target.y = move_toward(self.camera.target.y, self.lowest_target(), scroll_speed);
        } else {
            let above_row_top = (current_row - 1) as f32 * self.row_height + self.header_size;
            let below_row_bottom = (current_row + 1) as f32 * self.row_height + self.header_size + self.row_height;
            let camera_bottom = self.camera.target.y + LOGICAL_SCREEN_HEIGHT / 2.0;
            if above_row_top < self.camera_top() {
                let target = above_row_top + LOGICAL_SCREEN_HEIGHT / 2.0;
                self.camera.target.y = move_toward(self.camera.target.y, target, scroll_speed);
            } else if below_row_bottom > camera_bottom {
                let target = below_row_bottom - LOGICAL_SCREEN_HEIGHT / 2.0;
                self.camera.target.y = move_toward(self.camera.target.y, target, scroll_speed);
            }
        };
    }

    fn mouse_update<T: Enum + Copy>(&mut self, inputs: &InputHandler<T>) {
        let scroll_speed = 40.0;
        if inputs.mouse_wheel_rolled(MouseWheel::Up) {
            self.camera.target.y -= scroll_speed;
            self.camera.target.y = f32::max(self.camera.target.y, LOGICAL_SCREEN_HEIGHT / 2.0);
        } else if inputs.mouse_wheel_rolled(MouseWheel::Down) {
            self.camera.target.y += scroll_speed;
            self.camera.target.y = f32::min(self.camera.target.y, self.lowest_target());
        }
    }

    pub fn draw_scroll_bar(&self, style: &ScrollBarStyle) {
        let scroll_height = self.header_size + self.row_count as f32 * self.row_height;
        let bar_len_percent = LOGICAL_SCREEN_HEIGHT / scroll_height;
        let above_percent = self.camera_top() / scroll_height;
        let scroll_bar_top = LOGICAL_SCREEN_HEIGHT * above_percent + self.camera_top();
        draw_rectangle(
            LOGICAL_SCREEN_WIDTH - style.width,
            self.camera_top(),
            style.width,
            LOGICAL_SCREEN_HEIGHT,
            style.bg_color,
        );
        draw_rectangle(
            LOGICAL_SCREEN_WIDTH - style.width,
            scroll_bar_top,
            style.width,
            LOGICAL_SCREEN_HEIGHT * bar_len_percent,
            style.bar_color,
        );
    }

    pub fn camera_top(&self) -> f32 {
        self.camera.target.y - LOGICAL_SCREEN_HEIGHT / 2.0
    }

    pub fn scroll_to_top(&mut self) {
        self.scroll_state = ScrollState::ToTop;
    }

    pub fn scroll_to_bottom(&mut self) {
        self.scroll_state = ScrollState::ToBottom;
    }

    fn at_top(&self) -> bool {
        self.camera.target.y == LOGICAL_SCREEN_HEIGHT / 2.0
    }

    fn at_bottom(&self) -> bool {
        self.camera.target.y == self.lowest_target()
    }

    fn lowest_target(&self) -> f32 {
        self.header_size + self.row_count as f32 * self.row_height - LOGICAL_SCREEN_HEIGHT / 2.0
    }
}

fn move_toward(value: f32, target: f32, speed: f32) -> f32 {
    if value < target {
        if value + speed >= target { target } else { value + speed }
    } else if value - speed <= target {
        target
    } else {
        value - speed
    }
}
