#[cfg(not(target_arch = "wasm32"))]
pub fn setup_logging(level: log::LevelFilter, filename: &str) {
    use std::fs::File;

    use simplelog::*;

    let mut loggers: Vec<Box<dyn SharedLogger>> = vec![TermLogger::new(
        level,
        Config::default(),
        TerminalMode::Mixed,
        ColorChoice::Auto,
    )];
    std::fs::create_dir_all("logs").unwrap_or_else(|e| eprintln!("Failed create log directory. Error: {}", e));
    let file_logger =
        File::create(format!("logs/{}.log", filename)).map(|file| WriteLogger::new(level, Config::default(), file));
    match file_logger {
        Ok(file_logger) => loggers.push(file_logger),
        Err(e) => eprintln!(
            "Failed to initialize file logging for file '{}'. Error: {}",
            filename, e
        ),
    }
    CombinedLogger::init(loggers).unwrap_or_else(|e| eprintln!("Failed to initialize logging. Error: {}", e));
}

#[cfg(target_arch = "wasm32")]
pub fn setup_logging(level: log::LevelFilter, _filename: &str) {
    let Some(level) = level.to_level() else {
        return;
    };
    let _ = console_log::init_with_level(level);
}
