use std::path::Path;

use anyhow::Result;
use serde::{Serialize, de::DeserializeOwned};

#[cfg(not(target_arch = "wasm32"))]
pub fn load_save<T: DeserializeOwned>(file_path: impl AsRef<Path>) -> Result<T> {
    let data = std::fs::read_to_string(file_path)?;
    Ok(ron::de::from_str(&data)?)
}

#[cfg(not(target_arch = "wasm32"))]
pub fn save_save<T: Serialize>(data: &T, file_path: impl AsRef<Path>) -> Result<()> {
    let config = ron::ser::PrettyConfig::default().compact_arrays(true);
    let serialized = ron::ser::to_string_pretty(data, config)?;
    if let Some(parent) = file_path.as_ref().parent() {
        std::fs::create_dir_all(parent)?;
    }
    std::fs::write(&file_path, serialized)?;
    Ok(())
}

pub fn load_or_else<T, F: FnOnce() -> T>(path: &str, op: F) -> T
where
    T: Serialize + DeserializeOwned,
{
    match load_save(path) {
        Ok(data) => data,
        Err(e) => {
            log::error!("Error reading {path}: {e}\nUsing defaults.");
            let default = op();
            save_save(&default, path).unwrap_or_else(|e| log::error!("Failed to save default {path}: {e}"));
            default
        }
    }
}

#[cfg(target_arch = "wasm32")]
pub fn load_save<T: DeserializeOwned>(file_path: impl AsRef<Path>) -> Result<T> {
    use anyhow::anyhow;
    let file_path = file_path
        .as_ref()
        .to_str()
        .ok_or(anyhow!("Invalid path passed to load_save()."))?;

    let window = web_sys::window().ok_or(anyhow!("web_sys::window() failed."))?;

    let title = window
        .document()
        .map(|d| d.title())
        .unwrap_or_else(|| String::from("title_not_found"));
    let full_path = format!("{}/{}", title, file_path);

    let Ok(Some(local_storage)) = window.local_storage() else {
        return Err(anyhow!("Failed to access local_storage."));
    };

    let Ok(Some(data)) = local_storage.get_item(&full_path) else {
        return Err(anyhow!("File '{}' was not found in local_storage.", full_path));
    };

    Ok(ron::de::from_str(&data)?)
}

#[cfg(target_arch = "wasm32")]
pub fn save_save<T: Serialize>(data: &T, file_path: impl AsRef<Path>) -> Result<()> {
    use anyhow::anyhow;
    let serialized = ron::to_string(data)?;
    let file_path = file_path
        .as_ref()
        .to_str()
        .ok_or(anyhow!("Invalid path passed to load_save()."))?;

    let window = web_sys::window().ok_or(anyhow!("web_sys::window() failed."))?;

    let title = window
        .document()
        .map(|d| d.title())
        .unwrap_or_else(|| String::from("title_not_found"));
    let full_path = format!("{}/{}", title, file_path);

    let Ok(Some(local_storage)) = window.local_storage() else {
        return Err(anyhow!("Failed to access local_storage."));
    };

    local_storage
        .set_item(&full_path, &serialized)
        .map_err(|e| anyhow!("Failed to save item '{}' in local_storage. Error: {:?}", full_path, e))
}
