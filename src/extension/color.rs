use macroquad::{
    color::{hsl_to_rgb, rgb_to_hsl},
    color_u8,
    prelude::Color,
};

pub const fn hex_rgba(c: u32) -> Color {
    let c = c.to_be_bytes();
    color_u8!(c[0], c[1], c[2], c[3])
}

pub const fn hex_rgb(c: u32) -> Color {
    let c = c.to_be_bytes();
    color_u8!(c[1], c[2], c[3], 255)
}

pub trait ColorExtension {
    fn with_alpha(self, a: f32) -> Self;
    fn adjust_lightness(self, delta_l: f32) -> Self;
}

impl ColorExtension for Color {
    fn with_alpha(mut self, a: f32) -> Self {
        self.a = a;
        self
    }

    fn adjust_lightness(self, delta_l: f32) -> Self {
        let (h, s, mut l) = rgb_to_hsl(self);
        l = (l + delta_l).clamp(0.0, 1.0);
        hsl_to_rgb(h, s, l)
    }
}
