use macroquad::{
    camera::Camera2D,
    math::{Rect, vec2},
};

pub static LOGICAL_SCREEN_WIDTH: f32 = 1280.0;
pub static LOGICAL_SCREEN_HEIGHT: f32 = 720.0;

pub trait CameraExtension {
    fn get_screen_rect(&self) -> Rect;
}

impl CameraExtension for Camera2D {
    fn get_screen_rect(&self) -> Rect {
        let top_left = self.screen_to_world(vec2(0.0, 0.0));
        Rect::new(top_left.x, top_left.y, LOGICAL_SCREEN_WIDTH, LOGICAL_SCREEN_HEIGHT)
    }
}

impl CameraExtension for Option<&Camera2D> {
    fn get_screen_rect(&self) -> Rect {
        match self {
            Some(camera) => camera.get_screen_rect(),
            None => Rect::new(0.0, 0.0, LOGICAL_SCREEN_WIDTH, LOGICAL_SCREEN_HEIGHT),
        }
    }
}

pub fn build_screen_camera() -> Camera2D {
    let mut camera = Camera2D::from_display_rect(Rect::new(0.0, 0.0, LOGICAL_SCREEN_WIDTH, -LOGICAL_SCREEN_HEIGHT));
    camera.target = vec2(LOGICAL_SCREEN_WIDTH / 2.0, LOGICAL_SCREEN_HEIGHT / 2.0);
    camera
}
