use std::fmt::Display;

use enum_map::Enum;
use include_dir::Dir;

pub mod animation;
pub mod audio;
pub mod graphics;

pub trait AssetType: Clone + Copy + Enum + Display + strum::IntoEnumIterator {}

pub enum LoadDirectory {
    Relative(&'static str),
    Included(Dir<'static>),
}
