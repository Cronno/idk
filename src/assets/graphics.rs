use std::sync::LazyLock;

use anyhow::{Result, anyhow};
use enum_map::EnumMap;
use macroquad::{text::Font, texture::Texture2D};
use serde::de::DeserializeOwned;

use super::{AssetType, LoadDirectory, animation::SpriteData};

pub static NOT_FOUND_TEXTURE: LazyLock<Texture2D> =
    LazyLock::new(|| Texture2D::from_file_with_format(include_bytes!("../../assets/gfx/not_found.png"), None));

pub static CIRCLE_TEXTURE: LazyLock<Texture2D> =
    LazyLock::new(|| Texture2D::from_file_with_format(include_bytes!("../../assets/gfx/circle.png"), None));

#[derive(Debug)]
pub struct Assets<Gfx, Sheet, Ttf>
where
    Gfx: AssetType,
    Sheet: AssetType,
    Ttf: AssetType,
{
    gfx: EnumMap<Gfx, Option<Texture2D>>,

    sprite_sheets: EnumMap<Sheet, Option<Texture2D>>,
    sprite_data: EnumMap<Sheet, Option<SpriteData>>,

    fonts: EnumMap<Ttf, Option<Font>>,
}

impl<Gfx, Sheet, Ttf> Assets<Gfx, Sheet, Ttf>
where
    Gfx: AssetType,
    Sheet: AssetType,
    Ttf: AssetType,
{
    pub async fn load(load_dir: &LoadDirectory) -> Self {
        log::info!("Loading Graphics...");
        let mut gfx = EnumMap::default();
        for item in Gfx::iter() {
            gfx[item] = match load_texture(&format!("gfx/{item}.png"), load_dir).await {
                Ok(gfx) => Some(gfx),
                Err(e) => {
                    log::error!("Failed to load: assets/gfx/{item}.png - {e}");
                    None
                }
            }
        }

        let mut sprite_sheets = EnumMap::default();
        let mut sprite_data = EnumMap::default();
        for item in Sheet::iter() {
            sprite_sheets[item] = match load_texture(&format!("gfx/{item}.png"), load_dir).await {
                Ok(gfx) => Some(gfx),
                Err(e) => {
                    log::error!("Failed to load: assets/gfx/{item}.png - {e}");
                    None
                }
            };

            sprite_data[item] = match load_data(&format!("gfx/{item}.ron"), load_dir).await {
                Ok(data) => Some(data),
                Err(e) => {
                    log::error!("Failed to load: assets/gfx/{item}.ron - {e}");
                    None
                }
            }
        }

        log::info!("Loading Fonts...");
        let mut fonts = EnumMap::default();
        for item in Ttf::iter() {
            fonts[item] = match load_font(&format!("fonts/{item}.ttf"), load_dir).await {
                Ok(font) => Some(font),
                Err(e) => {
                    log::error!("Failed to load: assets/fonts/{item}.ttf - {e}");
                    None
                }
            }
        }

        Self {
            gfx,
            sprite_sheets,
            sprite_data,
            fonts,
        }
    }

    pub fn gfx(&self, item: Gfx) -> &Texture2D {
        match &self.gfx[item] {
            Some(texture) => texture,
            None => &NOT_FOUND_TEXTURE,
        }
    }

    pub fn sprite_sheets(&self, item: Sheet) -> &Texture2D {
        match &self.sprite_sheets[item] {
            Some(sheet) => sheet,
            None => &NOT_FOUND_TEXTURE,
        }
    }

    pub fn sprite_data(&self, item: Sheet) -> &SpriteData {
        match &self.sprite_data[item] {
            Some(data) => data,
            None => panic!(),
        }
    }

    pub fn fonts(&self, item: Ttf) -> Option<&Font> {
        match &self.fonts[item] {
            Some(font) => Some(font),
            None => panic!(),
        }
    }
}

pub async fn load_data<T: DeserializeOwned>(file_path: &str, load_dir: &LoadDirectory) -> Result<T> {
    let data = match load_dir {
        LoadDirectory::Relative(dir) => {
            let file_path = format!("{dir}/{file_path}");
            &macroquad::file::load_file(&file_path)
                .await
                .map_err(|e| anyhow!("Error opening {file_path}: {e}"))?
        }
        LoadDirectory::Included(dir) => dir
            .get_file(file_path)
            .ok_or_else(|| anyhow!("Error opening {file_path}"))?
            .contents(),
    };
    ron::de::from_bytes(data).map_err(|e| anyhow!("Error reading {file_path}: {e}"))
}

pub async fn load_texture(file_path: &str, load_dir: &LoadDirectory) -> Result<Texture2D> {
    match load_dir {
        LoadDirectory::Relative(dir) => macroquad::texture::load_texture(&format!("{dir}/{file_path}"))
            .await
            .map_err(|e| anyhow!(e)),
        LoadDirectory::Included(dir) => {
            let data = dir
                .get_file(file_path)
                .ok_or_else(|| anyhow!("Texture not found: {file_path}"))?;
            Ok(Texture2D::from_file_with_format(data.contents(), None))
        }
    }
}

pub async fn load_font(file_path: &str, load_dir: &LoadDirectory) -> Result<Font> {
    match load_dir {
        LoadDirectory::Relative(dir) => macroquad::text::load_ttf_font(&format!("{dir}/{file_path}"))
            .await
            .map_err(|e| anyhow!(e)),
        LoadDirectory::Included(dir) => {
            let data = dir
                .get_file(file_path)
                .ok_or_else(|| anyhow!("Font not found: {file_path}"))?;
            macroquad::text::load_ttf_font_from_bytes(data.contents()).map_err(|e| anyhow!(e))
        }
    }
}
