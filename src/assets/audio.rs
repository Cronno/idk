use anyhow::{Result, anyhow};
use enum_map::EnumMap;
use macroquad::audio::{PlaySoundParams, Sound, play_sound, play_sound_once, stop_sound};

use super::{AssetType, LoadDirectory};

pub struct Audio<Bgm, Sfx>
where
    Bgm: AssetType,
    Sfx: AssetType,
{
    bgm: EnumMap<Bgm, Option<Sound>>,
    sfx: EnumMap<Sfx, Option<Sound>>,
    pub mute: bool,
}

impl<Bgm, Sfx> Audio<Bgm, Sfx>
where
    Bgm: AssetType,
    Sfx: AssetType,
{
    pub async fn load(load_dir: &LoadDirectory) -> Self {
        log::info!("Loading Audio...");
        let mut bgm = EnumMap::default();
        for item in Bgm::iter() {
            let path = format!("bgm/{item}.ogg");
            bgm[item] = match load_audio(&path, load_dir).await {
                Ok(bgm) => Some(bgm),
                Err(e) => {
                    log::error!("Failed to load: assets/{path} - {e}");
                    None
                }
            }
        }

        let mut sfx = EnumMap::default();
        for item in Sfx::iter() {
            let path = format!("sfx/{item}.ogg");
            sfx[item] = match load_audio(&path, load_dir).await {
                Ok(sfx) => Some(sfx),
                Err(e) => {
                    log::error!("Failed to load: assets/{path} - {e}");
                    None
                }
            }
        }
        Self { bgm, sfx, mute: false }
    }

    pub fn play(&self, sfx: Sfx) {
        if !self.mute {
            match &self.sfx[sfx] {
                Some(sound) => play_sound_once(sound),
                None => log::error!("Sound Effect not loaded: {sfx}"),
            };
        }
    }

    pub fn start(&self, bgm: Bgm) {
        if !self.mute {
            match &self.bgm[bgm] {
                Some(sound) => play_sound(
                    sound,
                    PlaySoundParams {
                        looped: true,
                        ..Default::default()
                    },
                ),
                None => log::error!("Music not loaded: {bgm}"),
            };
        }
    }

    pub fn stop(&self, bgm: Bgm) {
        match &self.bgm[bgm] {
            Some(sound) => stop_sound(sound),
            None => log::error!("Music not loaded: {bgm}"),
        };
    }
}

pub async fn load_audio(file_path: &str, load_dir: &LoadDirectory) -> Result<Sound> {
    match load_dir {
        LoadDirectory::Relative(dir) => macroquad::audio::load_sound(&format!("{dir}/{file_path}"))
            .await
            .map_err(|e| anyhow!(e)),
        LoadDirectory::Included(dir) => {
            let data = dir
                .get_file(file_path)
                .ok_or_else(|| anyhow!("Audio not found: {file_path}"))?;
            macroquad::audio::load_sound_from_bytes(data.contents())
                .await
                .map_err(|e| anyhow!(e))
        }
    }
}
