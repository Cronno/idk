use std::{collections::HashMap, time::Duration};

use macroquad::{
    prelude::{RED, Rect, Vec2, WHITE},
    shapes::draw_rectangle_lines,
    texture::{DrawTextureParams, draw_texture_ex},
};
use serde::{Deserialize, Serialize};
use serde_with::{DurationMilliSeconds, serde_as};

use super::{AssetType, graphics::Assets};
use crate::{extension::rect::RectDef, misc::timer::Timer};

#[serde_as]
#[derive(Debug, Serialize, Deserialize)]
pub struct SpriteData {
    #[serde_as(as = "Vec<RectDef>")]
    frames: Vec<Rect>,
    animations: HashMap<String, HashMap<String, Vec<Keyframe>>>,
}

#[serde_as]
#[derive(Debug, Serialize, Deserialize)]
struct Keyframe {
    frame: usize,
    #[serde_as(as = "HashMap<_, RectDef>")]
    hitboxes: HashMap<String, Rect>,
    #[serde_as(as = "DurationMilliSeconds")]
    duration: Duration,
    x: f32,
    y: f32,
}

pub struct Sprite<Sheet> {
    sheet: Sheet,
    animation: String,
    pub direction: String,
    current_frame: usize,
    frame_timer: Timer,
}

impl<Sheet> Sprite<Sheet>
where
    Sheet: AssetType,
{
    pub fn new<Gfx, Ttf>(anim: Sheet, animation: &str, direction: &str, assets: &Assets<Gfx, Sheet, Ttf>) -> Self
    where
        Gfx: AssetType,
        Ttf: AssetType,
    {
        let animation = animation.to_string();
        let direction = direction.to_string();
        let duration = assets.sprite_data(anim).animations[&animation][&direction][0].duration;
        let frame_timer = Timer::new(duration);
        Self {
            sheet: anim,
            animation,
            direction,
            current_frame: 0,
            frame_timer,
        }
    }

    pub fn advance<Gfx, Ttf>(&mut self, delta: Duration, assets: &Assets<Gfx, Sheet, Ttf>)
    where
        Gfx: AssetType,
        Ttf: AssetType,
    {
        self.frame_timer.tick(delta);
        while self.frame_timer.finished() {
            let frames = &assets.sprite_data(self.sheet).animations[&self.animation][&self.direction];
            self.current_frame += 1;
            self.current_frame %= frames.len();
            let overflow = self.frame_timer.overflow();
            self.frame_timer = Timer::new(frames[self.current_frame].duration);
            self.frame_timer.tick(overflow);
        }
    }

    pub fn draw<Gfx, Ttf>(&self, pos: &Vec2, show_hitboxes: bool, assets: &Assets<Gfx, Sheet, Ttf>)
    where
        Gfx: AssetType,
        Ttf: AssetType,
    {
        let sprite_data = assets.sprite_data(self.sheet);
        let frame_data = &sprite_data.animations[&self.animation][&self.direction][self.current_frame];
        let frame = sprite_data.frames[frame_data.frame];

        draw_texture_ex(
            assets.sprite_sheets(self.sheet),
            pos.x + frame_data.x,
            pos.y + frame_data.y,
            WHITE,
            DrawTextureParams {
                source: Some(frame),
                ..Default::default()
            },
        );

        if show_hitboxes {
            for hitbox in frame_data.hitboxes.values() {
                draw_rectangle_lines(pos.x + hitbox.x, pos.y + hitbox.y, hitbox.w, hitbox.h, 5.0, RED);
            }
        }
    }

    pub fn get_hitboxes<'a, Gfx, Ttf>(&self, assets: &'a Assets<Gfx, Sheet, Ttf>) -> &'a HashMap<String, Rect>
    where
        Gfx: AssetType,
        Ttf: AssetType,
    {
        &assets.sprite_data(self.sheet).animations[&self.animation][&self.direction][self.current_frame].hitboxes
    }
}
